using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
 
    class Program
    {
     

          static void Main()
          {
            Console.WriteLine("Calculate the total, percentage and division to take marks of three subjects:\n-------------------------------------------------");
            Console.Write("Input the Roll Number of the student: ");
            int roll = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input the Name of the Student: ");
            string name = Console.ReadLine();
            Console.Write("Input the marks of Physics: ");
            int phmark = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input the marks of Chemistry: ");
            int chmark = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input the marks of Conputer Application: ");
            int camark = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("----------------------------------------------");
            Console.WriteLine("Roll No: " + roll);
            Console.WriteLine("Name of Student: " + name);
            Console.WriteLine("Marks in Physics: " + phmark);
            Console.WriteLine("Marks in Chemistry: " + chmark);
            Console.WriteLine("Marks in Computer Application: " + camark);
            Console.WriteLine("Total Marks = " + (phmark + chmark + camark));
            Console.WriteLine("Percentage = " + (phmark + chmark + camark)/3.0);
            if((phmark + chmark + camark) / 3.0 >= 60)
                Console.WriteLine("Division = First");
            else if((phmark + chmark + camark) / 3.0 < 60 && (phmark + chmark + camark) / 3.0 >= 48)
                Console.WriteLine("Division = Second");
            else if ((phmark + chmark + camark) / 3.0 < 48 && (phmark + chmark + camark) / 3.0 >= 36)
                Console.WriteLine("Division = Pass");
            else
                Console.WriteLine("Division = Fail");


        }


    }

}
