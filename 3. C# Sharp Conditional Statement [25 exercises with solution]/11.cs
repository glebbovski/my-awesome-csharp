using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
 
    class Program
    {
     

          static void Main()
          {
            Console.Write("Input the value of a: ");
            double a = Convert.ToDouble(Console.ReadLine());
            Console.Write("Input the value of b: ");
            double b = Convert.ToDouble(Console.ReadLine());
            Console.Write("Input the value of c: ");
            double c = Convert.ToDouble(Console.ReadLine());

            double d = b * b - 4 * a * c;

            if(d == 0)
            {
                double x = (-b + Math.Sqrt(d)) / 2 * a;
                Console.WriteLine("We have one solution:");
                Console.WriteLine("x = {0}", x);
            } else if (d > 0)
            {
                Console.WriteLine("We have several solutions:");
                double x = (-b + Math.Sqrt(d)) / 2 * a;
                Console.WriteLine("x1 = {0}", x);
                x = (-b - Math.Sqrt(d)) / 2 * a;
                Console.WriteLine("x2 = {0}", x);
            }
            else
            {
                Console.WriteLine("d less then 0 => no solution");
            }





            
          }


    }

}
