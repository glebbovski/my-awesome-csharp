using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
 
    class Program
    {
     

          static void Main()
        {
            Console.WriteLine("Input the value of temperature: ");
            string str = Console.ReadLine();
            int temp = Convert.ToInt32(str);

            if (temp < 0)
            {
                Console.WriteLine("Freezing weather");
            } else if (temp >= 0 && temp <= 10)
            {
                Console.WriteLine("Very Cold weather");
            }else if (temp > 10 && temp <= 20)
            {
                Console.WriteLine("Cold weather");
            }else if(temp > 20 && temp <= 30)
            {
                Console.WriteLine("Normal weather");
            }else if(temp > 30 && temp <= 40)
            {
                Console.WriteLine("Its Hot");
            }else if(temp > 40)
            {
                Console.WriteLine("Its Very Hot");
            } else
            {
                Console.WriteLine("Try again");
            }

        }


    }

}
