using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
 
    class Program
    {


        static void Main()
        {
            Console.WriteLine("Input your angles: ");
            Console.Write("First: ");
            int a1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Second: ");
            int a2 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Third: ");
            int a3 = Convert.ToInt32(Console.ReadLine());

            if(a1 + a2 + a3 != 180 || a1 <= 0 || a2 <= 0 || a3 <= 0)
                Console.WriteLine("The triangle is not valid.");
            else
                Console.WriteLine("The triangle is valid.");


        }


    }

}
