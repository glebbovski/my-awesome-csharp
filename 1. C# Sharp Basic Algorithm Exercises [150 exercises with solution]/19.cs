using System;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter your FIRST integer or 99999 to end it: ");
                int first = Convert.ToInt32(Console.ReadLine());
                if (first == 99999)
                    break;
                Console.WriteLine("Enter your SECOND integer or 99999 to end it: ");
                int second = Convert.ToInt32(Console.ReadLine());
                if (second == 99999)
                    break;
                Console.WriteLine("Your integers are " + first + ", " + second);

                Console.WriteLine(task(first, second));

               




            }
           
        }

        public static int task(int f, int s)
        {
            if (Math.Abs(100 - f) < Math.Abs(100 - s)) return f;
            else if (Math.Abs(100 - f) > Math.Abs(100 - s)) return s;
            else return 0;


        }
    }
}
