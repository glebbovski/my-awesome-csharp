using System;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Enter your count of elements of array or 99999 to end it: ");
                int count = Convert.ToInt32(Console.ReadLine());
                if (count == 99999) break;
                Random r = new Random();
                int[] arr = new int[count];
                for (int i = 0; i < count; i++)
                    arr[i] = r.Next(10);
                Console.Write("Enter the integer which you want to check or 99999 to end it: ");
                int check = Convert.ToInt32(Console.ReadLine());
                if (check == 99999) break;
                Console.Write("Your array is: {");
                for(int i = 0; i < count - 1; i++)
                {
                    Console.Write(arr[i] + ", ");
                }
                Console.Write(arr[count - 1] + "}\n");
                Console.WriteLine("Your integer is " + check);
                Console.WriteLine("\nResult: " + task(arr, check) + "\n");

                
                
                
              
            }
           
        }

        public static bool task(int[] arr, int check)
        {
            for (int i = 0; i < arr.Length; i++)
                if (arr[i] == check) return true;


            return false;

        }
    }
}
