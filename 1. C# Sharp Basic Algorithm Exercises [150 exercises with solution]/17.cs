using System;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter your string or \"plsno\" to end it: ");
                string str = Console.ReadLine();
                if (str == "plsno")
                    break;
                else
                    Console.WriteLine(task(str));


            }
           
        }

        public static string task(string str)
        {
            return str.Substring(1, 2).Contains("yt") ? str.Remove(1, 2) : str;
        }
    }
}
