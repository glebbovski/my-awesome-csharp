using System;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                Console.WriteLine("Please enter your first integer to comparing or 99999 to end it: ");
                int first = Convert.ToInt32(Console.ReadLine());
                if (first == 99999)
                    break;
                Console.WriteLine("Please enter your second integer or 99999 to end it: ");
                int second = Convert.ToInt32(Console.ReadLine());
                if (second == 99999)
                    break;
                if((first >= 100 && first < 200) || (second >= 100 && second < 200))
                    Console.WriteLine("\nTrue\n");
                else
                    Console.WriteLine("\nFalse\n");

            }
            

        }
    }
}
