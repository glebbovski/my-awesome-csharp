using System;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                Console.Write("Enter your integer or 123456 to escape: ");
                int i = Convert.ToInt32(Console.ReadLine());
                if(i % 3 == 0 || i % 7 == 0)
                    Console.WriteLine("True");
                else
                {
                    Console.WriteLine("False");
                }

                if (i == 123456)
                    break;
            }
        }
    }
}
