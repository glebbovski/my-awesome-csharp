using System;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter your string or \"plsno\" to end it: ");
                string str = Console.ReadLine();
                if (str.Contains("plsno"))
                    break;
                Console.WriteLine("Enter your integer of copies or 99999 to end it: ");
                int integer = Convert.ToInt32(Console.ReadLine());
                if (integer == 99999)
                    break;
                string trs = task(str, integer);
                Console.WriteLine(trs);
                
              
            }
           
        }

        public static string task(string str, int integer)
        {
            string s = "";
            for (int i = 0; i < integer; i++)
                s += str;
                   
            return s;

        }
    }
}
