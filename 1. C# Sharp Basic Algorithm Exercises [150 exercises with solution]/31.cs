using System;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter your string or \"plsno\" to end it: ");
                string str = Console.ReadLine();
                if (str.Contains("plsno"))
                    break;
                Console.WriteLine(task(str));
                
                
                
              
            }
           
        }

        public static int task(string str)
        {
            int q = 0;
            string last_two_chars = str.Substring(str.Length - 2, 2);
            for (int i = 0; i < str.Length - 2; i++)
                if (str.Substring(i, 2).Equals(last_two_chars))
                    q++;
            
            return q;

        }
    }
}
