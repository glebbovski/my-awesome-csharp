using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            

            while (true)
            {
                Console.Write("Enter count of elements in array or 99999 to end it: ");
                int count = Convert.ToInt32(Console.ReadLine());
                if (count == 99999) break;
                int[] arr = new int[count];
                Console.Write("Array: { ");
                Random r = new Random();
                for (int i = 0; i < count; i++)
                    arr[i] = r.Next(50);
                for (int i = 0; i < count - 1; i++)
                    Console.Write(arr[i] + ", ");
                Console.WriteLine(arr[count - 1] + " }");
                Console.WriteLine(task(arr));

            }
           
        }

        public static bool task(int[] arr)
        {
            if (arr[0] == arr[arr.Length - 1]) return true;

            return false;

        }


    }
}
