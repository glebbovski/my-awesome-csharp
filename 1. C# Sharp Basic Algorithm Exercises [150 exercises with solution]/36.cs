using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {


              while (true)
              {
                Console.Write("Enter your string or \"plsno\" to end it: ");
                string str = Console.ReadLine();
                if (str.Contains("plsno")) break;
                Console.Write("Enter the character which do you want to delete: ");
                char x = Convert.ToChar(Console.ReadLine());
                Console.WriteLine(task(str, x));

              }
 

        }

        public static string task(string str, char x) 
        {
            for(int i = 1; i < str.Length - 1; i++)
            {
                if (str[i] == x) str = str.Remove(i, 1);
                
            }
            
 
            return str;
        }


    }
}
