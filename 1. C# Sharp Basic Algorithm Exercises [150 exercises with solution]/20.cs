using System;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter your FIRST integer or 99999 to end it: ");
                int first = Convert.ToInt32(Console.ReadLine());
                if (first == 99999)
                    break;
                Console.WriteLine("Enter your SECOND integer or 99999 to end it: ");
                int second = Convert.ToInt32(Console.ReadLine());
                if (second == 99999)
                    break;
                Console.WriteLine("Your integers are " + first + ", " + second);
                Console.WriteLine(task(first, second));

               




            }
           
        }

        public static bool task(int f, int s)
        {
            return (f >= 40 && f <= 50 && s >= 40 && s <= 50) || (f >= 50 && f <= 60 && s >= 50 && s <= 60);

        }
    }
}
