using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            

            while (true)
            {
                Console.Write("Enter your string or \"plsno\" to end it: ");
                string str = Console.ReadLine();
                if (str.Contains("plsno")) break;
                Console.WriteLine(task(str));

                

            }
           
        }

        public static string task(string str)
        {
            if (str.Length >= 3)
            {
                if (str.Substring(0, 3).Contains("xyz"))
                    return "xyz";
                if (str.Substring(0, 3).Contains("abc"))
                    return "abc";
            }

            return "";
        }


    }
}
