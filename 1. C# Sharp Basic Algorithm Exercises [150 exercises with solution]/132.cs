using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {

            int p = 0;
              while (true)
            {
                int[] arr = {1,2,5,3,5,4,6,9,11};

                int[] newarr = new int[arr.Length];

                Console.WriteLine("Old arr:");
                for (int i = 0; i < arr.Length; i++)
                {
                    Console.Write(arr[i] + " ");
                }
                Console.WriteLine();

                newarr = task(arr);
                Console.WriteLine("New arr:");
                for(int i = 0; i < arr.Length; i++)
                {
                    Console.Write(newarr[i] + " ");
                }
                Console.WriteLine();

                p++;
                if (p == 1) break;
                

            }
 

        }

        public static int[] task(int[] arr) 
        {
            int j = 0;
            for(int i = 0; i < arr.Length; i++)
            {
                if(arr[i] % 2 == 0)
                {
                    int b = arr[i];
                    arr[i] = arr[j];
                    arr[j] = b;
                    j++;
                }

            }
            return arr;
        }


    }
}
