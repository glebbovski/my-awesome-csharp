using System;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter your FIRST integer or 99999 to end it: ");
                int first = Convert.ToInt32(Console.ReadLine());
                if (first == 99999)
                    break;
                Console.WriteLine("Enter your SECOND integer or 99999 to end it: ");
                int second = Convert.ToInt32(Console.ReadLine());
                if (second == 99999)
                    break;
                Console.WriteLine("Enter your THIRD integer or 99999 to end it: ");
                int third = Convert.ToInt32(Console.ReadLine());
                if (third == 99999)
                    break;

                Console.WriteLine("The largest number among three given integers is " + task(first, second,third));




            }
           
        }

        public static int task(int f, int s, int t)
        {

            return f > s ? f > t ? f : t : s > t ? s : t;
        }
    }
}
