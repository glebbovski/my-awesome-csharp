using System;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                Console.WriteLine("Enter your string or \"plsno\" if you want to end it: ");
                string str = Console.ReadLine();
                if (str.Length <= 3)
                {
                    for (int i = 0; i < 3; i++)
                        Console.Write(str);
                    Console.WriteLine();
                }
                else
                {
                    string tsr = str.Remove(3) + str + str.Remove(3);
                    Console.WriteLine(tsr);

                }
                

                if (str == "plsno")
                    break;

            }
            

        }
    }
}
