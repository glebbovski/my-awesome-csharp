using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input first number: ");
            int first = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input operation: ");
            char operation = Convert.ToChar(Console.ReadLine());
            Console.Write("Input second number: ");
            int second = Convert.ToInt32(Console.ReadLine());
            double res;

            switch(operation)
            {
                case '-':
                    res = first - second;
                    break;
                case '+':
                    res = first + second;
                    break;
                case '*': 
                    res = first * second;
                    break;
                case '/':
                    res = first / second;
                    break;
                default:
                    res = 0;
                    break;

            }
            Console.WriteLine(first + " " + operation + " " + second + " = " + res);



        }

        


    }
}
