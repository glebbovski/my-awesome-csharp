using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter your symbol: ");
            char symb = Convert.ToChar(Console.ReadLine());

            if (Char.IsDigit(symb))
            {
                Console.WriteLine("It's a digit");
            }

           else if (Char.IsLower(symb))
            {
               
               if (Char.IsLetter(symb))
                {
                    if (symb == 'e' || symb == 'u' || symb == 'i' || symb == 'o' || symb == 'y' || symb == 'a')
                    {
                        Console.WriteLine("It's a lowercase vowel");
                    }
                    else
                    {
                        Console.WriteLine("It's a lowercase consonant");
                    }


                }
                
            }
            else
            {
                Console.WriteLine("It's an other symbol");
            }


        }

    }
}
