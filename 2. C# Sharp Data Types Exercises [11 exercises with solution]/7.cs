using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input distance(metres): ");
            double metres = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input timeSec(hour): ");
            double hour = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input timeSec(minutes): ");
            double minutes = Convert.ToInt32(Console.ReadLine());
            Console.Write("Input timeSec(seconds): ");
            double seconds = Convert.ToInt32(Console.ReadLine());

            double metsec = metres / (hour * 60 * 60 + minutes * 60 + seconds);
            double kmh = metres / (1000 * (hour + minutes / 60 + seconds / 3600)); 
            double milh = kmh / 1.609;

            Console.WriteLine("Your speed in metres/sec is " + metsec);
            Console.WriteLine("Your speed in km/h is " + kmh);
            Console.WriteLine("Your speed in miles/h is " +  milh);





        }

        


    }
}
