using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Input the radius of the circle: ");
            double radius = Convert.ToDouble(Console.ReadLine());
            double S = Math.PI * Math.Pow(radius, 2); //площадь
            double P = 2 * Math.PI * radius; //периметр
            Console.WriteLine($"Perimetr: {P: 0.00}");
            Console.WriteLine("Area: {0:0.00} ", S);



        }

        


    }
}
