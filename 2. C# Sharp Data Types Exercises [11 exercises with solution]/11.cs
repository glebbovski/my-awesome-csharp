using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter your decimal integer: ");
            int dec = Convert.ToInt32(Console.ReadLine());
            string binary = "";
            while(dec > 0)
            {
                int q = dec % 2;
                binary += Convert.ToString(q);
                dec = dec / 2;
            }
           
            for(int i = 0; i < binary.Length; i++)
                Console.Write(binary[binary.Length - i - 1]);
            Console.WriteLine();

        }


        

    }
}
