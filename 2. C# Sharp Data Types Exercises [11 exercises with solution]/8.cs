using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Radius: ");
            double rad = Convert.ToDouble(Console.ReadLine());
            double S = 4 * Math.PI * rad * rad;
            double V = 4f / 3f * Math.PI * rad * rad * rad;
            Console.WriteLine("Surface: {0:0.00000}", S);
            Console.WriteLine("Volume: {0:0.00000}", V);


        }

    }
}
