using System;
using System.Collections.Generic;
using System.Linq;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("x = y^2 - 2y + 1");
            int x, y;
            for(y = -5; y <= 5; y++)
            {
                x = y * y - 2 * y + 1;
                Console.WriteLine("y = " + y + " ; x = (" + y + ")^2 - 2 * (" + y + ") + 1 = " + x);
            }



        }

        


    }
}
